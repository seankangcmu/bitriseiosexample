//
//  bitriseiosexampleApp.swift
//  bitriseiosexample
//
//  Created by Sean Kang on 11/28/20.
//

import SwiftUI

@main
struct bitriseiosexampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
